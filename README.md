<p></p>
<p></p>

<p align="center">
<img src="pyminer/resources/logo/logo.png" height="80"/> 
</p>


<div align="center">


[![Platform](https://img.shields.io/badge/python-v3.8-blue)](https://img.shields.io/badge/python-v3.8-blue)
[![Platform](https://img.shields.io/badge/PySide2-blue)](https://img.shields.io/badge/PySide2-blue)
[![License](https://img.shields.io/badge/license-LGPL-blue)](https://img.shields.io/badge/license-LGPL-blue)

</div>

<div align="center">
    <a src="https://img.shields.io/badge/QQ%e7%be%a4-orange">
        <img src="https://img.shields.io/badge/QQ%e7%be%a4-945391275-orange">
    </a>
</div>

<p></p>
<p></p>

<div align="center">
<h2>pyminer</h2>
<h3> 开源、友好、跨平台的数据分析解决方案</h3>

</div>
pyminer 是一款基于Python的开源、跨平台数据分析环境。它以方便Python初学者为己任，在Python的知识理论和工作实践之间搭建桥梁，竭诚为初学者服务。

它开箱即用，大大减少配置解释器环境的繁琐性。不仅提供了编程运行的功能，还能够以交互式的形式进行常见的数据分析操作，减少代码编写和文档查阅的时间。

## 🎉 有关近期事项的说明（2022年6月）

目前PyMiner组织的成员，正在做以下事情：
- 支持Ligral仿真语言的开发
- 开发基于网页的、图形化的仿真系统，方便用户使用

我们的工作内容涉及如下两个仓库：

- 支持[Ligral仿真系统的**核心**程序及文本建模语言](https://gitee.com/junruoyu-zheng/ligral) 的开发。此仓库目前由 [junruoyu-zheng](https://gitee.com/junruoyu-zheng) 开发和维护。
- 开发 [基于Ligral的网页可视化仿真系统](https://gitee.com/py2cn/ligralpy), 由PyMiner开发组成员开发和维护

此仿真系统类似于Simulink,可以实现控制仿真等功能，效果如下图所示：

![](./pics/flowchart-editor.png)

![](./pics/simulation-result.png)

我们转变方向的核心原因是，我们最终发现数据处理平台已有众多成熟的解决方案，而科学计算软件内置的算法才是最重要的价值所在。在一系列算法工具箱中，
最为重要的功能当属 Simulink，它被广泛用于控制系统仿真，在工业界、学术界有着几乎不可撼动的地位。

同为PyMiner核心开发者的 junruoyu-zheng 开发了基于C#的Ligral，这样便有了核心算法程序。如果有一个用户友好的界面，便可以让更多的人来认识Ligral。
这也是我们发起网页版仿真系统的初衷。

## 🎉 技术说明

- pyminer 的官方发行版本为Python3.8+PySide2-5.15.2。开发者可自行使用其他版本的Python解释器配置相关环境。
- pyminer 曾经由PyQt5开发。但考虑到官方支持以及许可证的类型，我们已经迁移到了PySide2并改变许可证为LGPL。请勿使用PyQt5安装。
- 当使用Python3.8配置环境时，不支持3.8.0等低版本的Python3.8解释器。当使用Python3.8时，请使用3.8.5或者更高版本的解释器。

- 如果使用出现问题，欢迎提issue。




## 🚄 开源地址

- Gitee：[https://gitee.com/py2cn/pyminer](https://gitee.com/py2cn/pyminer)
- GitHub：[https://github.com/aboutlong/pyminer](https://github.com/pyminer/pyminer)



## 🥂 安装体验


### 发行版下载（仅Windows系统）
我们为Windows系统的用户提供了发行版的下载链接，你可以在我们的官网中下载发行版即刻体验。对于Mac OS和Linux系统的用户，暂时不提供发行版，可以参阅“开发者自行安装”一节。

官网链接：[http://www.pyminer.com](http://www.pyminer.com/)

### 开发者自行安装（适合Windows、Mac OS以及各大Linux发行版）
#### 安装前准备：
1. 确认你的Python解释器版本。pyminer支持3.5~3.9。
	- 当使用Python3.8.x时，建议x>=5,也就是使用Python3.8.5及以上版本，否则安装PySide2可能遇到问题
	- 3.5.x/3.6.x/3.7.x/3.9.x下，由于开发人员不足，未进行充分测试。为稳定起见，建议解释器版本x>=5。
2. 建议新建全新的虚拟环境，尤其是当旧的虚拟环境中存在其他依赖于PyQt/PySide2的程序时。pyminer与这些程序可能发生依赖冲突。


#### Windows安装 pyminer

第一步：安装python 3.8或以上版本
第二步：安装pyminer

```bash

pip install pyminer

```


## 🥂 加入我们

作者：pyminer Development Team<br>
邮箱：team@pyminer.com

欢迎各位开发者大佬加入 


##  📸 预览截图

基本界面
![avatar](pyminer/resources/screenshot/main.png)

代码提示
![avatar](pyminer/resources/screenshot/code.png)

绘图
![avatar](pyminer/resources/screenshot/check_data.png)

